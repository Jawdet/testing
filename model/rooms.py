# -*- coding: utf-8 -*-
__author__ = 'bobrov'

# модель сущности предметной области
class Rooms:
    def __init__(self, entrance, room_number, floor_number, total_area, living_area):

        self.entrance = entrance
        self.room_number = room_number
        self.floor_number = floor_number
        self.total_area = total_area
        self.living_area = living_area

