# -*- coding: utf-8 -*-
__author__ = 'bobrov'

# модель сущности предметной области
class New_house:
    def __init__(self, district=None, street=None, house_number=None, postal_index=None, tech_status=None, maintenance_date=None,
                        build_date=None, surrounding_area=None, roof_area=None, attic_area=None, cellar_area=None, stairs_area=None):

        self.district = district
        self.street = street
        self.house_number = house_number
        self.postal_index = postal_index
        self.tech_status = tech_status
        self.maintenance_date = maintenance_date
        self.build_date = build_date
        self.surrounding_area = surrounding_area
        self.roof_area = roof_area
        self.attic_area = attic_area
        self.cellar_area = cellar_area
        self.stairs_area = stairs_area