# -*- coding: utf-8 -*-
__author__ = 'bobrov'

from model.new_house import New_house
from model.rooms import Rooms
import time


def test_add_house(app):
        app.new_house.fill_info(New_house(district="Железнодорожный", street="Тестовая", house_number="1",
                                          postal_index="630132", tech_status="90",
                                          maintenance_date="01.01.1992", build_date="01.01.1991",
                                          surrounding_area="500", roof_area="250", attic_area="250",
                                          cellar_area="250", stairs_area="300"))

        # Костыль, чтобы дать прогрузиться странице. Заменить нормальным ожиданием!
        time.sleep(3)

        app.rooms.fill_flats(Rooms(entrance="1", room_number="1", floor_number="1",
                                   total_area="45", living_area="28"))
