# -*- coding: utf-8 -*-
__author__ = 'bobrov'

from model.new_house import New_house


def test_modify_house(app):
    app.new_house.modify_first_house(New_house(district="Ленинский", street="ТестоваяИзмененная", house_number="2",
                                      postal_index="630054", tech_status="95", surrounding_area="600",
                                       roof_area="350", attic_area="350", cellar_area="350", stairs_area="200"))

