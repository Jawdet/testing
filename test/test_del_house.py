# -*- coding: utf-8 -*-
__author__ = 'bobrov'

from model.new_house import New_house


def test_delete_first_house(app):
    # Проверяет, если на странице нет элементов карточки "Общая информация", то создает новое здание
    if app.new_house.count() == 0:
        app.new_house.fill_info(New_house(district="Железнодорожный", street="Тестовая", house_number="1",
                                          postal_index="630132", tech_status="90",
                                          maintenance_date="01.01.1992", build_date="01.01.1991",
                                          surrounding_area="500", roof_area="250", attic_area="250",
                                          cellar_area="250", stairs_area="300"))
    app.new_house.delete_first_house()
