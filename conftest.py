# -*- coding: utf-8 -*-
__author__ = 'bobrov'

import pytest
from fixture.application import  Application

fixture = None

# создает объект Application и возвращает эту фикстуру. Проверяет валидность фикстуры перед кажлым тестом.
@pytest.fixture
def app(request):
    global fixture
    if fixture is None:
        fixture = Application()
    else:
        if not fixture.is_valid():
            fixture = Application()
    fixture.session.ensure_login(username="autotest", password="12345")
    return fixture

@pytest.fixture(scope="session", autouse=True)
def stop(request):
    def fin():
        fixture.session.ensure_logout()
        fixture.destroy()
    request.addfinalizer(fin)
    return fixture