# -*- coding: utf-8 -*-
__author__ = 'bobrov'


class NewHouseHelper:

    # В конструктор передается ссылка на application, главный класс фикстуры
    def __init__(self, app):
        self.app = app


    def open_new_houses_page(self):
        # Переход в "Новые здания"
        wd = self.app.wd
        if wd.current_url.endswith('/house-add'):
            return
        wd.find_element_by_xpath('/html/body/aside/nav/ul[1]/li[6]/a/span[1]').click()


    def fill_info(self, new_house):
        # Заполнение формы создания нового здания
        wd = self.app.wd
        self.open_new_houses_page()
        wd.find_element_by_css_selector('div[click\\.delegate="modalCreate(\'info\')"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.district"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.district"]').send_keys(new_house.district)
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.street"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.street"]').send_keys(new_house.street)
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.building"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.building"]').send_keys(new_house.house_number)
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.zipcode"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.zipcode"]').send_keys(new_house.postal_index)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'tech_status\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'tech_status\']"]').send_keys(new_house.tech_status)
        # Замена value инпута через js value
        element = wd.find_element_by_css_selector('input[value\\.bind="data.object[\'construction_date\']"]')
        wd.execute_script('arguments[0].value="01.01.1991";', element)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'construction_date\']"]').send_keys(new_house.build_date)
        element = wd.find_element_by_css_selector('input[value\\.bind="data.object[\'operation_date\']"]')
        wd.execute_script('arguments[0].value="01.01.1992";', element)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'operation_date\']"]').send_keys(new_house.maintenance_date)
        wd.find_element_by_css_selector('div[click\\.delegate="toggleObjectProp(data, \'infoAreas\')"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'surrounding_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'surrounding_area\']"]').send_keys(new_house.surrounding_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'roof_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'roof_area\']"]').send_keys(new_house.roof_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'attic_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'attic_area\']"]').send_keys(new_house.attic_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'cellar_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'cellar_area\']"]').send_keys(new_house.cellar_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'stairs_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'stairs_area\']"]').send_keys(new_house.stairs_area)
        self.create_new_house()

    def modify_info(self, new_house):
        # Редактирование "Общей информации" дома
        wd = self.app.wd
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.district"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.district"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.district"]').send_keys(
            new_house.district)
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.street"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.street"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.street"]').send_keys(new_house.street)
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.building"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.building"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.building"]').send_keys(
            new_house.house_number)
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.zipcode"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.zipcode"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object.address.zipcode"]').send_keys(
            new_house.postal_index)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'tech_status\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'tech_status\']"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'tech_status\']"]').send_keys(
            new_house.tech_status)
        wd.find_element_by_css_selector('div[click\\.delegate="toggleObjectProp(data, \'infoAreas\')"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'surrounding_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'surrounding_area\']"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'surrounding_area\']"]').send_keys(
            new_house.surrounding_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'roof_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'roof_area\']"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'roof_area\']"]').send_keys(
            new_house.roof_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'attic_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'attic_area\']"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'attic_area\']"]').send_keys(
            new_house.attic_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'cellar_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'cellar_area\']"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'cellar_area\']"]').send_keys(
            new_house.cellar_area)
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'stairs_area\']"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'stairs_area\']"]').clear()
        wd.find_element_by_css_selector('input[value\\.bind="data.object[\'stairs_area\']"]').send_keys(
            new_house.stairs_area)

    def open_new_house_general_info(self):
        wd = self.app.wd
        # Открытие карточки первого здания
        wd.find_element_by_css_selector('button[click\\.delegate="modalCreate(\'info\', h)"]').click()

    def delete_first_house(self):
        wd = self.app.wd
        self.open_new_houses_page()
        # Удаление первого здания
        wd.find_element_by_css_selector('button[click\\.delegate="delete(h)"]').click()

    def modify_first_house(self, new_house_data):
        wd = self.app.wd
        self.open_new_houses_page()
        self.open_new_house_general_info()
        self.modify_info(new_house_data)
        self.save_house_info()

    def create_new_house(self):
        # Создание нового здания
        wd = self.app.wd
        # Нажатие на кнопку "добавить"
        wd.find_element_by_xpath('//*[@id="modal_info"]/footer/div/button[1]').click()

    def save_house_info(self):
        # Сохранение изменений в "Общей информации"
        wd = self.app.wd
        wd.find_element_by_xpath('//*[@id="modal_info"]/footer/div/button[1]').click()

    def count(self):
        # Считает количество созданных зданий на странице
        wd = self.app.wd
        self.open_new_houses_page()
        return len('button[click\\.delegate="modalCreate(\'info\', h)"]')
