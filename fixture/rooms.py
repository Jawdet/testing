# -*- coding: utf-8 -*-
__author__ = 'bobrov'

from fixture.new_house import NewHouseHelper

class RoomsHelper(NewHouseHelper) :

    # В конструктор передается ссылка на application, главный класс фикстуры
    def __init__(self, app):
        self.app = app

    def fill_flats(self, rooms):
        # Заполнение формы создания помещений
        wd = self.app.wd
        self.open_new_houses_page()
        wd.find_element_by_css_selector('button[click\\.delegate="modalCreate(\'rooms\', h)"]').click()
        self.add_entrance()
        wd.find_element_by_css_selector('input[value\\.bind="entrance.number"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="entrance.number"]').send_keys(rooms.entrance)
        # Клик по чекбоксу "квартира"
        element = wd.find_elements_by_css_selector('input[change\\.delegate="signaler.signal(\'changedRTcheck\')"]')[0]
        wd.execute_script('arguments[0].click();', element)
        wd.find_element_by_css_selector('div[click\\.delegate="rt.expand = !rt.expand"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="room.number"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="room.number"]').send_keys(rooms.room_number)
        wd.find_element_by_css_selector('input[value\\.bind="room.floor"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="room.floor"]').send_keys(rooms.floor_number)
        wd.find_element_by_css_selector('input[value\\.bind="room.total_area | float"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="room.total_area | float"]').send_keys(rooms.total_area)
        wd.find_element_by_css_selector('input[value\\.bind="room.living_area | float"]').click()
        wd.find_element_by_css_selector('input[value\\.bind="room.living_area | float"]').send_keys(rooms.living_area)
        self.save_entrance()


    def save_entrance(self):
        # Сохранение подьезда с помещениями
        wd = self.app.wd
        wd.find_elements_by_css_selector('button.btn-save[type="submit"]')[0].click()

    def add_entrance(self):
        # Нажатие "Добавить подьезд"
        wd = self.app.wd
        wd.find_element_by_css_selector('button[click\\.delegate="setEditRooms(data, \'new\')"]').click()
