# -*- coding: utf-8 -*-
__author__ = 'bobrov'

from selenium.webdriver.chrome.webdriver import WebDriver
#from selenium.webdriver.phantomjs.webdriver import WebDriver
from fixture.session import SessionHelper
from fixture.new_house import NewHouseHelper
from fixture.rooms import RoomsHelper


class Application:

    def __init__(self):
        self.wd = WebDriver()
        self.wd.implicitly_wait(20)
        self.wd.maximize_window()
        # Помощник получает ссылку на объект класса application
        self.session = SessionHelper(self)
        self.new_house = NewHouseHelper(self)
        self.rooms = RoomsHelper(self)

    def is_valid(self):
        try:
            self.wd.current_url
            return True
        except:
            return False

    def open_home_page(self):
        wd = self.wd
        wd.get("http://prod.amlsystems.ru/")


    def destroy(self):
        self.wd.quit()

