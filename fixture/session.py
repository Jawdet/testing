# -*- coding: utf-8 -*-
__author__ = 'bobrov'


# Помощник по работе с сессиями
class SessionHelper:

    # Конструктор в качестве параметра принимает ссылку на фикстуру application
    def __init__(self, app):
        self.app = app

    def login(self, username, password):
        wd = self.app.wd
        self.app.open_home_page()
        wd.find_element_by_id("LoginLogin").send_keys(username)
        wd.find_element_by_id("LoginPassword").send_keys(password)
        wd.find_element_by_xpath("//div[@class='login-body']/form/button").click()

    def logout(self):
        wd = self.app.wd
        wd.find_element_by_css_selector('a[click\\.delegate="toggleUserMenu()"]').click()
        wd.find_element_by_css_selector('a[click\\.delegate="logout()"]').click()

    def is_logged_in(self):
        wd = self.app.wd
        return len(wd.find_elements_by_css_selector('a[click\\.delegate="toggleUserMenu()"]')) > 0

    # Проверка, что пользователь по-прежнему залогинен в системе; если да, то выполняется logout
    def ensure_logout(self):
        wd = self.app.wd
        if self.is_logged_in():
            self.logout()

    # Проверка, если пользователь не залогинен; если да, то выполняется логин
    def ensure_login(self, username, password):
        wd = self.app.wd
        if self.is_logged_in():
            if self.is_logged_in_as(username):
                return
            else:
                self.logout()
        self.login(username, password)

    def is_logged_in_as(self, username):
        wd = self.app.wd
        return wd.find_element_by_css_selector('a[click\\.delegate="toggleUserMenu()"]').text == username


